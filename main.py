import tkinter as tk
from tkinter import messagebox, ttk
import openpyxl
from openpyxl import Workbook

class StudentPortal:
    def __init__(self, root):
        self.root = root
        self.root.title("Student Portal")

        # Create or load Excel file
        self.filename = "student_data.xlsx"
        self.create_excel_file()

        # Form
        self.label_name = tk.Label(root, text="Name:")
        self.entry_name = tk.Entry(root)

        self.label_roll = tk.Label(root, text="Roll Number:")
        self.entry_roll = tk.Entry(root)

        self.label_grade = tk.Label(root, text="Grade:")
        self.entry_grade = tk.Entry(root)

        self.label_marks = tk.Label(root, text="Marks:")
        self.entry_marks = tk.Entry(root)

        # Buttons
        self.button_submit = tk.Button(root, text="Submit", command=self.submit_data)
        self.button_reset = tk.Button(root, text="Reset Data", command=self.reset_data)

        # Grid placement
        self.label_name.grid(row=0, column=0, padx=10, pady=10)
        self.entry_name.grid(row=0, column=1, padx=10, pady=10)

        self.label_roll.grid(row=1, column=0, padx=10, pady=10)
        self.entry_roll.grid(row=1, column=1, padx=10, pady=10)

        self.label_grade.grid(row=2, column=0, padx=10, pady=10)
        self.entry_grade.grid(row=2, column=1, padx=10, pady=10)

        self.label_marks.grid(row=3, column=0, padx=10, pady=10)
        self.entry_marks.grid(row=3, column=1, padx=10, pady=10)

        self.button_submit.grid(row=4, column=0, columnspan=2, pady=10)
        self.button_reset.grid(row=6, column=0, columnspan=2, pady=10)

        # Table
        self.tree = ttk.Treeview(root, columns=("Name", "Roll Number", "Grade", "Marks"), show="headings")
        self.tree.heading("Name", text="Name")
        self.tree.heading("Roll Number", text="Roll Number")
        self.tree.heading("Grade", text="Grade")
        self.tree.heading("Marks", text="Marks")
        self.tree.grid(row=7, column=0, columnspan=2, padx=10, pady=10)


    def create_excel_file(self):
        try:
            # Try to load the existing workbook
            self.workbook = openpyxl.load_workbook(self.filename)
        except FileNotFoundError:
            # If the workbook doesn't exist, create a new one
            self.workbook = Workbook()
            self.workbook.save(self.filename)

    def submit_data(self):
        sheet = self.workbook.active
        data = [self.entry_name.get(), self.entry_roll.get(), self.entry_grade.get(), self.entry_marks.get()]
        sheet.append(data)
        self.workbook.save(self.filename)
        self.display_data()
        messagebox.showinfo("Success", "Data submitted successfully!")

    def display_data(self):
        try:
            sheet = self.workbook.active
            self.tree.delete(*self.tree.get_children())  # Clear existing entries

            for row in sheet.iter_rows(min_row=2, max_row=sheet.max_row, values_only=True):
                self.tree.insert("", tk.END, values=row)
        except Exception as e:
            messagebox.showerror("Error", f"An error occurred: {str(e)}")

    def reset_data(self):
        self.workbook.remove(self.workbook.active)  # Remove the existing sheet
        self.workbook.create_sheet()  # Create a new sheet
        self.workbook.save(self.filename)
        self.display_data()
        messagebox.showinfo("Success", "Data reset successfully!")

if __name__ == "__main__":
    root = tk.Tk()
    app = StudentPortal(root)
    root.mainloop()
